// Config Items

const config = {
	backgroundImage: 'zeekode-hipster.png',
	typedLines: [
		'Initializing web page',
		'Loading Zeekie',
		'Preparing speeches',
		'...',
		'Hiya, Welcome to Zeekode'
	],
	videoClips: [
		'videos/zyzy-clip-1.mp4',
		'videos/zyzy-clip-4.mp4',
		'videos/zyzy-clip-3.mp4',
		'videos/zyzy-clip-2.mp4'
	]
}

// FPS Meter

var meter = new FPSMeter(document.querySelector('#fps'), {
	interval:  32,     // Update interval in milliseconds.
	smoothing: 10,      // Spike smoothing strength. 1 means no smoothing.
	show:      'fps',   // Whether to show 'fps', or 'ms' = frame duration in milliseconds.
	decimals:  0,       // Number of decimals in FPS number. 1 = 59.9, 2 = 59.94, ...
	maxFps:    60,      // Max expected FPS value.
	threshold: 100,     // Minimal tick reporting interval in milliseconds.

	// Meter position
	zIndex:   10,         // Meter Z index.
	left:     '5px',      // Meter left offset.
	top:      '5px',      // Meter top offset.
	right:    'auto',     // Meter right offset.
	bottom:   'auto',     // Meter bottom offset.
	margin:   '0 0 0 0',  // Meter margin. Helps with centering the counter when left: 50%;

	// Theme
	theme: 'transparent', // Meter theme. Build in: 'dark', 'light', 'transparent', 'colorful'.

	// Graph
	graph:   1, // Whether to show history graph.
	history: 20 // How many history states to show in a graph.
})

// meter.tickStart()

const repeatOften = () => {
	meter.tick()

	requestAnimationFrame(repeatOften);
}

// requestAnimationFrame(repeatOften);

// P2P Communication

// Apply backgroune image

if (config.backgroundImage)
	document.body.querySelector('#background-image').src = `images/${config.backgroundImage}`

// Global colours

const setGlobalColours = (pri, sec, acc, cont, state) => {
	document.documentElement.style.setProperty('--clr-pri', pri)
	document.documentElement.style.setProperty('--clr-sec', sec)
	document.documentElement.style.setProperty('--clr-acc', acc)
	document.documentElement.style.setProperty('--clr-cont', cont)
	document.documentElement.style.setProperty('--clr-state', state)
}

const bgColours = (hue = 'random') => setGlobalColours(...randomColor({
	count: 5,
	luminosity: 'bright',
	hue
}))

bgColours()

// Video clips

const arrayRandomElem = arr => arr[~~(Math.random() * arr.length)]

const videoLoop = document.querySelector('#video-loop')

videoLoop.children[0].src = arrayRandomElem(config.videoClips)

videoLoop.addEventListener('ended', ({ target }) => {
	target.children[0].src = arrayRandomElem(config.videoClips)
	
	target.load()
	target.play()
})

// Text typing

const introTyping = document.querySelector('#intro-typing')

const textSpansTyped = config.typedLines
	.map((lines, i) => {
		const newSpan = document.createElement('span')

		newSpan.classList.add('textTyped')

		introTyping.appendChild(newSpan)

		new Typed(newSpan, {
			typeSpeed: 30,
			strings: [lines],
			startDelay: 3000 + (i * 3000), //Initial start delay and offsets
			showCursor: false
		})
	})

// Matrix code

const rainCanvas = document.getElementById("matrix");

let raining = null

const makeItRain = () => {
	if (raining) {
		clearInterval(raining)

		rainCanvas.style.opacity = 0

		raining = null
	} else {
		rainCanvas.style.opacity = 0.5
		
		raining = setInterval(matrixCode, 64)
	}
}

// Terminal console

const consoleLogic = (term, command) => {
	switch (command) {
		case 'help':
			term.print('Commands: game, clear, background, youtube')
			term.print('reset, matrix')
			break
		case 'background':
			bgColours()
			term.print('Setting new background colours')
			break
		case 'youtube':
			winTube.open()
			break
		case 'matrix':
			makeItRain()
			break
		case 'reset':
			location.reload()
			break
		case 'game':
			winGame.open()
			break
		case 'clear':
			term.clear()
		default:
			term.print('Type \'help\' for more information')
	}

	terminal.input('' , msg => consoleLogic(term, msg))
}

const later = (delay, value) =>
    new Promise(resolve => setTimeout(resolve, delay, value));

const terminal = new Terminal()

terminal.print('Welcome to Zeekode')

terminal.input('Welcome, type \'help\' for more information', msg => consoleLogic(terminal, msg))

// Window manager

const { innerWidth, innerHeight } = window

const percentWidth = perc => (innerWidth / 100) * perc

const percentHeight = perc => (innerHeight / 100) * perc 

const wm = new Ventus.WindowManager()

const winVids = wm.createWindow({
	title: '',
	x: percentWidth(75) - (400 / 2),
	y: percentHeight(30) - (250 / 2),
	width: 400,
	height: 250,
	resizable: false,
	titlebar: true,
	content: videoLoop,
	stayinspace: true,
	animations: false
})

const winTube = wm.createWindow({
	title: '',
	x: percentWidth(50) - (500 / 2),
	y: percentHeight(50) - (350 / 2),
	width: 500,
	height: 350,
	resizable: false,
	titlebar: true,
	content: '<iframe src="//www.youtube.com/embed/?listType=user_uploads&list=DevTipsForDesigners" width="500" height="315"></iframe>',
	stayinspace: true,
	animations: false
})

const winCode = wm.createWindow({
	title: '',
	x: 50,
	y: 50,
	width: 500,
	height: 250,
	resizable: false,
	titlebar: true,
	movable: false,
	content: terminal.html,
	classname: 'console',
	animations: false
})

const winGame = wm.createWindow({
	title: '',
	x: percentWidth(12),
	y: percentHeight(39),
	width: 350,
	height: 350,
	resizable: false,
	titlebar: true,
	content: '<canvas id=canvas-game width=350 height=350></canvas>',
	animations: false,
	events: {
		open (evt) {
			setTimeout(() => {
				canvasGame(document.body.querySelector('#canvas-game'))
			}, 0)
		}
	}
})

winVids.open()
winTube.open()
winCode.open()

